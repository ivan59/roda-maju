<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_customer',100)->nullable();
            $table->string('phone_customer',100)->nullable();
            $table->dateTime('service_date')->nullable();
            $table->dateTime('created_at')->nullable();
        });

        Schema::create('bookings_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bookings_id')->nullable();
            $table->integer('categories_detail_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
