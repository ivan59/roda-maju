<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BookingsController;
use App\Http\Controllers\LoginController;
use App\Models\Categories;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = Categories::getCategories();
    $minimumDate = date('Y-m-d', strtotime("+2 day"));
    return view('home', compact('categories','minimumDate'));
});
Route::post('submit', [BookingsController::class, 'submit'])->name('submit');
Route::post('process', [BookingsController::class, 'process'])->name('process');

Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::resource('posts', PostController::class)->middleware('auth');
Route::resource('users', UsersController::class)->middleware('auth');
Route::resource('categories', CategoriesController::class)->middleware('auth');
Route::resource('bookings', BookingsController::class)->middleware('auth');

Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
