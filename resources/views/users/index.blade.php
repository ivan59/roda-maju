@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <a class="btn btn-success" href="{{ route('users.create') }}"> Create User</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Created</th>
            <th width="280px"class="text-center">Action</th>
        </tr>
        @foreach ($users as $post)
        <tr>
            <td class="text-center">{{ ++$i }}</td>
            <td>{{ $post->name }}</td>
            <td>{{ $post->email }}</td>
            <td>{{ $post->username }}</td>
            <td>{{ $post->created_at }}</td>
            <td class="text-center">
                <form action="{{ route('users.destroy',$post->id) }}" method="POST">

                    <a class="btn btn-info btn-sm" href="{{ route('users.show',$post->id) }}">Show</a>

                    <a class="btn btn-primary btn-sm" href="{{ route('users.edit',$post->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $users->links() !!}

@endsection
