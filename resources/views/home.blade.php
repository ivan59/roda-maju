<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Roda Maju</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
            <div class="container px-5">
                <a class="navbar-brand fw-bold" href="#page-top">Roda Maju</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                        <li class="nav-item"><a class="nav-link me-lg-3" href="#booking">Booking Sekarang</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Mashead header-->
        <header class="masthead">
            <div class="container px-5">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <!-- Mashead text and app badges-->
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-1 lh-1 mb-3">Booking dengan aplikasi Roda Maju</h1>
                            <p class="lead fw-normal text-muted mb-5">Booking service kendaraan bisa dari mana aja, yuk download untuk mencoba kemudahan dalam booking service</p>
                            <div class="d-flex flex-column flex-lg-row align-items-center">
                                <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
                                <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="masthead-device-mockup">
                            <svg class="circle" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                                        <stop class="gradient-start-color" offset="0%"></stop>
                                        <stop class="gradient-end-color" offset="100%"></stop>
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50"></circle></svg
                            ><svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(120.42 -49.88) rotate(45)"></rect>
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(-49.88 120.42) rotate(-45)"></rect></svg
                            ><svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle cx="50" cy="50" r="50"></circle></svg>
                            <div class="device-wrapper">
                                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                                    <div class="screen bg-black">
                                        <video muted="muted" autoplay="" loop="" style="max-width: 100%; height: 100%"><source src="assets/img/demo-screen.mp4" type="video/mp4" /></video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- information-->
        <aside class="text-center bg-gradient-primary-to-secondary">
            <div class="container px-5">
                <div class="row gx-5 justify-content-center">
                    <div class="col-xl-8">
                        <div class="h2 fs-1 text-white mb-4">"Booking hanya bisa dilakukan minimal H-2 dari tanggal service"</div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- booking section-->
        <section id="booking">
            <div class="container px-5">
                <div class="row" id="myForm">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group mb-20">
                            <strong>Name Customer:</strong>
                            <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                            <input type="text" id="name_customer" name="name_customer" class="form-control" placeholder="Name Customer">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group mb-20">
                            <strong>Phone Customer:</strong>
                            <input type="number" id="phone_customer" name="phone_customer" class="form-control" placeholder="Phone Customer">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group mb-20">
                            <strong>Service Date:</strong>
                            <input type="date" id="service_date" name="service_date" class="form-control" min='{{$minimumDate}}'>
                        </div>
                    </div>

                    @if(count($categories)!=0)
                        @foreach($categories as $category)
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <strong>{{$category->name_category}}:</strong>
                                    @if(count($category->detail)!=0)
                                        @foreach($category->detail as $detail)
                                            <div class="form-check">
                                              <input class="form-check-input" name="services[]" type="checkbox" value="{{$detail->id}}" id="flexCheckDefault{{$detail->id}}">
                                              <label class="form-check-label" for="flexCheckDefault{{$detail->id}}">
                                                {{$detail->name_category_detail}}
                                              </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </section>
      
        <!-- Footer-->
        <footer class="bg-black text-center py-5">
            <div class="container px-5">
                <div class="text-white-50 small">
                    <div class="mb-2">&copy; Roda Maju 2022. All Rights Reserved.</div>
                    <a href="#!">Privacy</a>
                    <span class="mx-1">&middot;</span>
                    <a href="#!">Terms</a>
                    <span class="mx-1">&middot;</span>
                    <a href="#!">FAQ</a>
                </div>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

        <script>
            $('#submit').click( function() {
                var name_customer = $("#name_customer").val();
                var phone_customer = $("#phone_customer").val();
                var service_date = $("#service_date").val();
                var token = $("#token").val();
                var services = $("input[name='services[]']:checked").map(function(){return $(this).val();}).get();

                if(name_customer && phone_customer && service_date && services.length > 0) {
                    $.ajax({
                        url: "submit",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            "name_customer": name_customer,
                            "phone_customer": phone_customer,
                            "service_date": service_date,
                            "_token": token,
                            "services": services
                        },
                        success:function(response){
                            if(response.success) {
                                Swal.fire({
                                    type: 'success',
                                    title: 'Selamat',
                                    text: 'Sukses booking service, jika ingin cancel / mengubah jadwal, bisa hubungi 089XXXXXXXXX'
                                }).then (function() {
                                    window.location.href = "{{ url('/') }}";
                                });
                            }
                        }
                    });
                }else {
                    Swal.fire({
                        type: 'warning',
                        title: 'Peringatan',
                        text: 'Harap lengkapi semua field'
                    })
                }
            });
        </script>
    </body>
</html>