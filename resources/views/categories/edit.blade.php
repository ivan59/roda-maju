@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Category</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('categories.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('categories.update',$category->id) }}" method="POST">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name Category:</strong>
                    <input type="text" name="name_category" value="{{ $category->name_category }}" class="form-control" placeholder="Name">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <strong>Detail Service</strong>

                        @if(count($categoryDetails)!=0)
                            @foreach($categoryDetails as $detail)
                                <div class="form-group fieldGroup">
                                    <div class="input-group">
                                        <input type="text" name="categories_detail[]" class="form-control" placeholder="Name Detail Category" value="{{$detail->name_category_detail}}" />
                                        <div class="input-group-addon ml-3"> 
                                            <a href="javascript:void(0)" class="btn btn-success addMore">+</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <div class="form-group fieldGroup">
                            <div class="input-group">
                                <input type="text" name="categories_detail[]" class="form-control" placeholder="Name Detail Category"/>
                                <div class="input-group-addon ml-3"> 
                                    <a href="javascript:void(0)" class="btn btn-success addMore">+</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group fieldGroupCopy" style="display: none;">
                            <div class="input-group">
                                <input type="text" name="categories_detail[]" class="form-control" placeholder="Name Detail Category"/>
                                <div class="input-group-addon"> 
                                    <a href="javascript:void(0)" class="btn btn-danger remove">-</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>

    </form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        // membatasi jumlah inputan
        var maxGroup = 10;
        
        //melakukan proses multiple input 
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });
        
        //remove fields group
        $("body").on("click",".remove",function(){ 
            $(this).parents(".fieldGroup").remove();
        });
    });
</script>
@endsection