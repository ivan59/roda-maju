@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Show Category</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('categories.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name Category:</strong>
                {{ $category->name_category }}
            </div>

            <div class="form-group">
                <strong>Detail Category:</strong>
                <br>
                @if(count($categoryDetails)!=0)
                    @foreach($categoryDetails as $detail)
                        - {{ $detail->name_category_detail }} <br>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
