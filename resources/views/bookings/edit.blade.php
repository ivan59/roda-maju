@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Booking</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('bookings.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('bookings.update',$booking->id) }}" method="POST">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name Customer:</strong>
                    <input type="text" name="name_customer" class="form-control" placeholder="Name Customer" value="{{$booking->name_customer}}">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Phone Customer:</strong>
                    <input type="number" name="phone_customer" class="form-control" placeholder="Phone Customer" value="{{$booking->phone_customer}}">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Service Date:</strong>
                    <input type="date" name="service_date" class="form-control" value="{{date('Y-m-d', strtotime($booking->service_date))}}" min='{{$minimumDate}}'>
                </div>
            </div>

            @if(count($categories)!=0)
                @foreach($categories as $category)
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <strong>{{$category->name_category}}:</strong>
                            @if(count($category->detail)!=0)
                                @foreach($category->detail as $detail)
                                    <div class="form-check">
                                      <input class="form-check-input" name="services[]" type="checkbox" value="{{$detail->id}}" id="flexCheckDefault{{$detail->id}}" {{ in_array($detail->id, $selectedCategories) ? ' checked' : '' }}>
                                      <label class="form-check-label" for="flexCheckDefault{{$detail->id}}">
                                        {{$detail->name_category_detail}}
                                      </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>

    </form>
@endsection