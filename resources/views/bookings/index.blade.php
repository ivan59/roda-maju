@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <a class="btn btn-success" href="{{ route('bookings.create') }}"> Create Booking</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Name Customer</th>
            <th>Phone Customer</th>
            <th>Service Date</th>
            <th>Created</th>
            <th>Process</th>
            <th width="280px"class="text-center">Action</th>
        </tr>
        @foreach ($bookings as $booking)
        <tr>
            <td class="text-center">{{ ++$i }}</td>
            <td>{{ $booking->name_customer }}</td>
            <td>{{ $booking->phone_customer }}</td>
            <td>{{ date('Y-m-d', strtotime($booking->service_date)) }}</td>
            <td>{{ $booking->created_at }}</td>
            <td>
                <form action="{{ route('process') }}" method="POST">
                    @csrf
                    @if($booking->is_processing != 'yes')
                        <input type="hidden" name="booking_id" value="{{$booking->id}}">
                        <button type="submit" class="btn btn-warning btn-sm">Process</button>
                    @endif
                </form>
            </td>
            <td class="text-center">
                <form action="{{ route('bookings.destroy',$booking->id) }}" method="POST">
                    <a class="btn btn-info btn-sm" href="{{ route('bookings.show',$booking->id) }}">Show</a>
                    <a class="btn btn-primary btn-sm" href="{{ route('bookings.edit',$booking->id) }}">Edit</a>
                    @csrf
                    @method('DELETE')
                    @if(!$booking->is_processing)
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $bookings->links() !!}

@endsection
