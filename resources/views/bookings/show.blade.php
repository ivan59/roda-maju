@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Show Category</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('bookings.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name Customer:</strong>
                {{ $booking->name_customer }}
            </div>

            <div class="form-group">
                <strong>Phone Customer:</strong>
                {{ $booking->phone_customer }}
            </div>

            <div class="form-group">
                <strong>Service Date:</strong>
                {{ date('Y-m-d', strtotime($booking->service_date)) }}
            </div>

            <div class="form-group">
                <strong>Created:</strong>
                {{ $booking->created_at }}
            </div>

            <div class="form-group">
                <strong>Services:</strong>
                <br>
                @if(count($servicesDetails)!=0)
                    @foreach($servicesDetails as $detail)
                        - {{ $detail->name_category_detail }} <br>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
