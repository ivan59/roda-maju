<!DOCTYPE html>
<html>
<head>
    <title>Roda Maju</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="#">Roda Maju</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item {{Request::segment(1) == 'users' ? 'active' : ''}} ">
                    <a class="nav-link" href="{{url('users')}}">Admin</a>
                  </li>
                  <li class="nav-item {{Request::segment(1) == 'categories' ? 'active' : ''}}">
                    <a class="nav-link" href="{{url('categories')}}">Category</a>
                  </li>
                  <li class="nav-item {{Request::segment(1) == 'bookings' ? 'active' : ''}}">
                    <a class="nav-link" href="{{url('bookings')}}">Booking</a>
                  </li>
                </ul>
                <span class="navbar-text">
                  <a href="{{route('actionlogout')}}">Logout</a>
                </span>
              </div>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    @yield('content')
</div>

</body>
</html>
