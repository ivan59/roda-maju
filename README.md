<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Cara Installasi

- Buka Terminal / CMD
- Clone project <code>https://gitlab.com/ivan59/roda-maju.git</code>
- cd roda-maju
- Ketik <code>composer install</code>
- Kemudian ketik <code>cp .env.example .env</code>
- Ketik <code>php artisan key:generate</code>
- Buat database baru bernama <code>db_laravel8_crud</code>
- import database dengan file yang bernama : db_laravel8_crud.sql
- Konfigurasi database di <code>.env</code>
- Untuk frontend buka browser dan ketik <code>http://localhost:8888/roda-maju/public/</code>
- Untuk backend buka browser dan ketik <code>http://localhost:8888/roda-maju/public/login</code>
	* akun backend = username : ivan & password : 123456