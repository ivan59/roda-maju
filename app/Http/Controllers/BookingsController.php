<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Bookings;
use App\Models\BookingsDetail;

class BookingsController extends Controller
{
    public function index()
    {
        $bookings = Bookings::latest()->paginate(5);
        return view('bookings.index',compact('bookings'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $categories = Categories::getCategories();
        $minimumDate = date('Y-m-d', strtotime("+2 day"));
        return view('bookings.create',compact('categories','minimumDate'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_customer' => 'required',
            'phone_customer' => 'required',
            'service_date' => 'required',
        ]);

        $booking = Bookings::create($request->all());
        $bookingId = $booking['id'];

        $servicesDetails = $request['services'];
        if(!empty($servicesDetails)){
            for($a = 0; $a < count($servicesDetails); $a++){
                if(!empty($servicesDetails[$a])){
                    $value = $servicesDetails[$a];

                    $insert['bookings_id'] = $bookingId;
                    $insert['categories_detail_id'] = $value;
                    BookingsDetail::create($insert);
                }
            }
        }

        return redirect()->route('bookings.index')
                        ->with('success','Category created successfully.');
    }

    public function show(Bookings $booking)
    {
        $bookingId = $booking['id'];
        $servicesDetails = BookingsDetail::getServicesSelected($bookingId);
        return view('bookings.show',compact('booking','servicesDetails'));
    }

    public function edit(Bookings $booking)
    {
        $bookingId = $booking['id'];
        $categories = Categories::getCategories();
        $selectedCategories = BookingsDetail::getServicesSelectedPluck($bookingId);
        $minimumDate = date('Y-m-d', strtotime("+2 day"));
        return view('bookings.edit',compact('booking','categories','selectedCategories','minimumDate'));
    }

    public function update(Request $request, Bookings $booking)
    {
        $request->validate([
            'name_customer' => 'required',
            'phone_customer' => 'required',
            'service_date' => 'required',
        ]);

        $booking->update($request->all());
        $bookingId = $booking['id'];

        // delete
        BookingsDetail::where('bookings_id',$bookingId)->delete();

        // insert
        $servicesDetails = $request['services'];
        if(!empty($servicesDetails)){
            for($a = 0; $a < count($servicesDetails); $a++){
                if(!empty($servicesDetails[$a])){
                    $value = $servicesDetails[$a];

                    $insert['bookings_id'] = $bookingId;
                    $insert['categories_detail_id'] = $value;
                    BookingsDetail::create($insert);
                }
            }
        }

        return redirect()->route('bookings.index')
                        ->with('success','Booking updated successfully');
    }

    public function destroy(Bookings $booking)
    {
        $booking->delete();
        BookingsDetail::where('bookings_id',$booking['id'])->delete();

        return redirect()->route('bookings.index')
                        ->with('success','Booking deleted successfully');
    }

    public function process(Request $request)
    {
        $update['is_processing'] = 'yes';
        Bookings::where('id',$request['booking_id'])->update($update);

        return redirect()->route('bookings.index')
                        ->with('success','Booking process successfully');
    }

    public function submit(Request $request)
    {
        $save['name_customer'] = $request['name_customer'];
        $save['phone_customer'] = $request['phone_customer'];
        $save['service_date'] = $request['service_date'];
        $save['created_at'] = date('Y-m-d H:i:s');
        $booking = Bookings::create($save);
        $bookingId = $booking['id'];

        $servicesDetails = $request['services'];
        if(!empty($servicesDetails)){
            for($a = 0; $a < count($servicesDetails); $a++){
                if(!empty($servicesDetails[$a])){
                    $value = $servicesDetails[$a];

                    $insert['bookings_id'] = $bookingId;
                    $insert['categories_detail_id'] = $value;
                    BookingsDetail::create($insert);
                }
            }
        }

        return response()->json([
            'success' => true
        ], 200);

    }
}
