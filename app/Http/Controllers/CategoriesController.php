<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\CategoriesDetail;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Categories::latest()->paginate(5);

        return view('categories.index',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_category' => 'required',
        ]);

        $category = Categories::create($request->all());
        $categoryId = $category['id'];

        $categoryDetails = $request['categories_detail'];
        if(!empty($categoryDetails)){
            for($a = 0; $a < count($categoryDetails); $a++){
                if(!empty($categoryDetails[$a])){
                    $nameCategoryDetail = $categoryDetails[$a];

                    $insert['categories_id'] = $categoryId;
                    $insert['name_category_detail'] = $nameCategoryDetail;
                    CategoriesDetail::create($insert);
                }
            }
        }

        return redirect()->route('categories.index')
                        ->with('success','Category created successfully.');
    }

    public function show(Categories $category)
    {
        $categoryDetails = CategoriesDetail::where('categories_id', $category['id'])->get();
        return view('categories.show',compact('category','categoryDetails'));
    }

    public function edit(Categories $category)
    {
        $categoryDetails = CategoriesDetail::where('categories_id', $category['id'])->get();
        return view('categories.edit',compact('category','categoryDetails'));
    }

    public function update(Request $request, Categories $category)
    {
        $request->validate([
            'name_category' => 'required',
        ]);

        $category->update($request->all());
        $categoryId = $category['id'];

        // delete
        CategoriesDetail::where('categories_id',$categoryId)->delete();

        // insert
        $categoryDetails = $request['categories_detail'];
        if(!empty($categoryDetails)){
            for($a = 0; $a < count($categoryDetails); $a++){
                if(!empty($categoryDetails[$a])){
                    $nameCategoryDetail = $categoryDetails[$a];

                    $insert['categories_id'] = $categoryId;
                    $insert['name_category_detail'] = $nameCategoryDetail;
                    CategoriesDetail::create($insert);
                }
            }
        }

        return redirect()->route('categories.index')
                        ->with('success','Category updated successfully');
    }

    public function destroy(Categories $category)
    {
        $category->delete();
        CategoriesDetail::where('categories_id',$category['id'])->delete();

        return redirect()->route('categories.index')
                        ->with('success','Category deleted successfully');
    }
}
