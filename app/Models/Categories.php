<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_category',
    ];

    public static function getCategories()
    {
        $categories = Categories::get();
        if(count($categories)!=0) {
            foreach($categories as &$category) {
                $detailCategory = Categories::join('categories_detail','categories_detail.categories_id','=','categories.id')->where('categories_id',$category->id)->get();

                $category->detail = $detailCategory;
            }
        }

        return $categories;
    }
}
