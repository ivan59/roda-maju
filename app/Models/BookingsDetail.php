<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingsDetail extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'bookings_detail';

    protected $fillable = [
        'bookings_id',
        'categories_detail_id',
    ];

    public static function getServicesSelected($bookingId)
    {
        $services = BookingsDetail::join('categories_detail','categories_detail.id','=','bookings_detail.categories_detail_id')->where('bookings_id',$bookingId)->get();
        return $services;
    }

    public static function getServicesSelectedPluck($bookingId)
    {
        $services = BookingsDetail::join('categories_detail','categories_detail.id','=','bookings_detail.categories_detail_id')->where('bookings_id',$bookingId)->pluck('categories_detail_id')->toArray();
        return $services;
    }
}
