<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriesDetail extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'categories_detail';

    protected $fillable = [
        'categories_id',
        'name_category_detail',
    ];
}
